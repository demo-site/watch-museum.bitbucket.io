$('.js-showpiece-slider').owlCarousel({
    items: 1,
    loop: true,
    nav: false,
    dots: true,
    dotsSpeed: 500,
    smartSpeed: 500
});

var player = new Plyr('#player', {
    controls: ['play-large', 'play', 'progress', 'rewind', 'fast-forward'],
    tooltips: {
        controls: false,
        seek: false
    }
});

var playerVideo = new Plyr('#player_video', {
    controls: ['play-large', 'play', 'rewind', 'fast-forward', 'progress', 'current-time', 'fullscreen']
});