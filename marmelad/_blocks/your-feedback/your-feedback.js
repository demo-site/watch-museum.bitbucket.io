function feedbackSend() {
    $('.js-feedback-send').on('submit', function (event) {
        event.preventDefault();
        $('.js-popup-success').fadeIn();
    });
    
    $('.js-btn-close').on('click', function () {
        $('.js-popup-success').fadeOut();
        $('.js-value-clean').val('');
    });
}

function resizeTextArea() {
    $('#text_feedback').on('keyup paste', function () {
        var $this = $(this),
            offset = $this.innerHeight() - $this.height();
    
        if ($this.innerHeight() < this.scrollHeight) {
            
            $this.height(this.scrollHeight - offset);
        } else {
            
            $this.height(1);
            $this.height(this.scrollHeight - offset);
        }
    });
}

$('.phone-us').mask('+7 (000) 000-00-00');


feedbackSend();

resizeTextArea();