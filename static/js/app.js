"use strict";

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

/* ^^^
 * Глобальные-вспомогательные функции
 * ========================================================================== */

/**
  * Возвращает HTML-код иконки из SVG-спрайта
  *
  * @param {String} name Название иконки из спрайта
  * @param {Object} opts Объект настроек для SVG-иконки
  *
  * @example SVG-иконка
  * getSVGSpriteIcon('some-icon', {
  *   tag: 'div',
  *   type: 'icons', // colored для подключения иконки из цветного спрайта
  *   class: '', // дополнительные классы для иконки
  *   mode: 'inline', // external для подключаемых спрайтов
  *   url: '', // путь до файла спрайта, необходим только для подключаемых спрайтов
  * });
  */
function getSVGSpriteIcon(name, opts) {
  opts = _extends({
    tag: 'div',
    type: 'icons',
    "class": '',
    mode: 'inline',
    url: ''
  }, opts);
  var external = '';
  var typeClass = '';

  if (opts.mode === 'external') {
    external = "".concat(opts.url, "/sprite.").concat(opts.type, ".svg");
  }

  if (opts.type !== 'icons') {
    typeClass = " svg-icon--".concat(opts.type);
  }

  opts["class"] = opts["class"] ? " ".concat(opts["class"]) : '';
  return "\n    <".concat(opts.tag, " class=\"svg-icon svg-icon--").concat(name).concat(typeClass).concat(opts["class"], "\" aria-hidden=\"true\" focusable=\"false\">\n      <svg class=\"svg-icon__link\">\n        <use xlink:href=\"").concat(external, "#").concat(name, "\"></use>\n      </svg>\n    </").concat(opts.tag, ">\n  ");
}
/* ^^^
 * JQUERY Actions
 * ========================================================================== */


$(function () {
  'use strict';
  /**
   * определение существования элемента на странице
   */

  $.exists = function (selector) {
    return $(selector).length > 0;
  };

  $('.js-about-us-slider').owlCarousel({
    items: 1,
    loop: true,
    nav: false,
    dots: true // mouseDrag: false,
    // animateOut: 'fadeOut',
    // animateIn: 'slideOutin',

  });
  $('.js-btn-ok').on('click', function () {
    $(this).closest('.cookies').fadeOut();
  });
  var PAGE = $('html, body');
  var pageScroller = $('.page-scroller');
  var inMemoryClass = 'page-scroller--memorized';
  var isVisibleClass = 'page-scroller--visible';
  var enabledOffset = 60;
  var pageYOffset = 0;
  var inMemory = false;

  function resetPageScroller() {
    setTimeout(function () {
      if (window.pageYOffset > enabledOffset) {
        pageScroller.addClass(isVisibleClass);
      } else if (!pageScroller.hasClass(inMemoryClass)) {
        pageScroller.removeClass(isVisibleClass);
      }
    }, 150);

    if (!inMemory) {
      pageYOffset = 0;
      pageScroller.removeClass(inMemoryClass);
    }

    inMemory = false;
  }

  if (pageScroller.length > 0) {
    window.addEventListener('scroll', resetPageScroller, window.supportsPassive ? {
      passive: true
    } : false);
    pageScroller.on('click', function (event) {
      event.preventDefault();
      window.removeEventListener('scroll', resetPageScroller);

      if (window.pageYOffset > 0 && pageYOffset === 0) {
        inMemory = true;
        pageYOffset = window.pageYOffset;
        pageScroller.addClass(inMemoryClass);
        PAGE.stop().animate({
          scrollTop: 0
        }, 500, 'swing', function () {
          window.addEventListener('scroll', resetPageScroller, window.supportsPassive ? {
            passive: true
          } : false);
        });
      } else {
        pageScroller.removeClass(inMemoryClass);
        PAGE.stop().animate({
          scrollTop: pageYOffset
        }, 500, 'swing', function () {
          pageYOffset = 0;
          window.addEventListener('scroll', resetPageScroller, window.supportsPassive ? {
            passive: true
          } : false);
        });
      }
    });
  }

  $('.js-showpiece-slider').owlCarousel({
    items: 1,
    loop: true,
    nav: false,
    dots: true,
    dotsSpeed: 500,
    smartSpeed: 500
  });
  var player = new Plyr('#player', {
    controls: ['play-large', 'play', 'progress', 'rewind', 'fast-forward'],
    tooltips: {
      controls: false,
      seek: false
    }
  });
  var playerVideo = new Plyr('#player_video', {
    controls: ['play-large', 'play', 'rewind', 'fast-forward', 'progress', 'current-time', 'fullscreen']
  });

  function feedbackSend() {
    $('.js-feedback-send').on('submit', function (event) {
      event.preventDefault();
      $('.js-popup-success').fadeIn();
    });
    $('.js-btn-close').on('click', function () {
      $('.js-popup-success').fadeOut();
      $('.js-value-clean').val('');
    });
  }

  function resizeTextArea() {
    $('#text_feedback').on('keyup paste', function () {
      var $this = $(this),
          offset = $this.innerHeight() - $this.height();

      if ($this.innerHeight() < this.scrollHeight) {
        $this.height(this.scrollHeight - offset);
      } else {
        $this.height(1);
        $this.height(this.scrollHeight - offset);
      }
    });
  }

  $('.phone-us').mask('+7 (000) 000-00-00');
  feedbackSend();
  resizeTextArea();

  function showMenu() {
    $('.js-btn-burger').on('click', function () {
      $('.js-main-menu').addClass('show-main-menu');
      $('body').addClass('scroll-lock');
    });
    $('.js-btn-close').on('click', function () {
      $('.js-main-menu').removeClass('show-main-menu');
      $('body').removeClass('scroll-lock');
    });
  }

  showMenu();
  var centerLocationMap = {
    lat: 55.748163,
    lng: 37.605970
  };

  function locationMap() {
    var locationMapId = document.getElementById('location_map');

    if (!locationMapId) {
      return;
    }

    var map = new google.maps.Map(locationMapId, {
      center: {
        lat: centerLocationMap.lat,
        lng: centerLocationMap.lng
      },
      disableDefaultUI: true,
      zoom: 16
    });
    var marker = new google.maps.Marker({
      position: {
        lat: centerLocationMap.lat,
        lng: centerLocationMap.lng
      },
      map: map,
      icon: 'img/'
    });
  }

  locationMap();
});